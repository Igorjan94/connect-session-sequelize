/**
 * Sequelize based session store.
 *
 * Author: Michael Weibel <michael.weibel@gmail.com>
 * License: MIT
 */

const path = require('path')
const debug = require('debug')('connect:session-sequelize')
const defaultOptions = {
    checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions.
    expiration: 24 * 60 * 60 * 1000, // The maximum age (in milliseconds) of a valid session. Used when cookie.expires is not set.
    disableTouch: false // When true, we will not update the db in the touch function call. Useful when you want more control over db writes.
}

class SequelizeStoreException extends Error {
    constructor (message) {
        super(message)
        this.name = 'SequelizeStoreException'
    }
}

module.exports = function SequelizeSessionInit (Store) {
    class SequelizeStore extends Store {
        constructor (options) {
            super(options)
            this.options = options = options || {}

            this.options = Object.assign(defaultOptions, this.options)
            this.startExpiringSessions()

            this.sessionModel = options.db[options.table] || options.db.models[options.table]
        }

        sync() {
        }

        async get(id, fn) {
            debug('SELECT "%s"', id)
            let session = await this.sessionModel.findById(id);
            if (!session) {
                debug('Did not find session %s', id)
                return fn(null, null);
            }
            debug('FOUND %s with data %s', session.id, session.data)

            fn(null, JSON.parse(session.data));
        }

        async set(id, data, fn) {
            debug('INSERT "%s"', id)
            const expires = this.expiration(data)
            const realData = data;
            realData.expires = expires;

            const defaults = {
                id,
                expires,
                data: JSON.stringify(realData)
            };

            let session = await this.sessionModel.findById(id)
            if (!session)
                session = await this.sessionModel.create(defaults)
            else
                session = await this.sessionModel.updateAttributes(defaults);

            fn(null, session.data);
        }

        async touch(id, data, fn) {
            debug('TOUCH "%s"', id)

            if (this.options.disableTouch) {
                debug('TOUCH skipped due to disableTouch "%s"', id)
                return fn(null, null)
            }

            const expires = this.expiration(data)
            let session = await this.sessionModel.updateAttributes({ expires, id })
            fn(null, null)
        }

        async destroy(id, fn) {
            debug('DESTROYING %s', id)
            let session = await this.sessionModel.findById(id)
            if (session === null) {
                debug('Session not found, assuming destroyed %s', id)
                return fn(null, null)
            }
            session = await this.sessionModel.destroy(session.id)
            fn(null, session)
        }

        length(fn) {
            return this.sessionModel.count().then(fn)
        }

        async clearExpiredSessions(fn) {
            debug('CLEARING EXPIRED SESSIONS')
            await this.sessionModel.destroy({ where: { expires: { lt: Date.parse(new Date()) } } })
        }

        startExpiringSessions () {
        // Don't allow multiple intervals to run at once.
            this.stopExpiringSessions()
            if (this.options.checkExpirationInterval > 0) {
                this._expirationInterval = setInterval(
                    this.clearExpiredSessions.bind(this),
                    this.options.checkExpirationInterval
                )
                // allow to terminate the node process even if this interval is still running
                this._expirationInterval.unref()
            }
        }

        stopExpiringSessions () {
            if (this._expirationInterval) {
                clearInterval(this._expirationInterval)
                // added as a sanity check for testing
                this._expirationInterval = null
            }
        }

        expiration (data) {
            if (data.cookie && data.cookie.expires)
                return data.cookie.expires
            return Date.parse(new Date(Date.now() + this.options.expiration))
        }
    }

    return SequelizeStore
}