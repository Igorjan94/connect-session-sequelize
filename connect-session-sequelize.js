/**
 * Sequelize based session store.
 *
 * Author: Michael Weibel <michael.weibel@gmail.com>
 * License: MIT
 */

var util = require('util')
var path = require('path')
var debug = require('debug')('connect:session-sequelize')
var defaultOptions = {
  checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions.
  expiration: 24 * 60 * 60 * 1000  // The maximum age (in milliseconds) of a valid session. Used when cookie.expires is not set.
}

function SequelizeStoreException (message) {
  this.name = 'SequelizeStoreException'
  this.message = message
  Error.call(this)
}
util.inherits(SequelizeStoreException, Error)

module.exports = function SequelizeSessionInit (Store) {
  function SequelizeStore (options) {
    this.options = options = options || {}
    if (!options.db) {
      throw new SequelizeStoreException('Database connection is required')
    }

    for (var key in defaultOptions) {
      if (key in options === false) {
        this.options[key] = defaultOptions[key]
      }
    }

    Store.call(this, options)

    this.startExpiringSessions()

    // Check if specific table should be used for DB connection
    if (options.table) {
      debug('Using table: %s for sessions', options.table)
      // Get Specifed Table from Sequelize Object
      this.sessionModel = options.db[options.table] || options.db.models[options.table]
    } else {
      // No Table specified, default to ./model
      debug('No table specified, using default table.')
      this.sessionModel = options.db.import(path.join(__dirname, 'model'))
    }
  }

  util.inherits(SequelizeStore, Store)

  SequelizeStore.prototype.sync = function sync () {
    return this.sessionModel.sync()
  }

  SequelizeStore.prototype.get = function getSession (id, fn) {
    debug('SELECT "%s"', id)
    return this.sessionModel.findById(id).then(function (session) {
      if (!session) {
        debug('Did not find session %s', id)
        return null
      }
      debug('FOUND %s with data %s', session.id, session.data)

      return JSON.parse(session.data)
    })
  }

  SequelizeStore.prototype.set = async function setSession (id, data, fn) {
    debug('INSERT "%s"', id)
    var stringData = JSON.stringify(data)
    var expires

    if (data.cookie && data.cookie.expires) {
      expires = data.cookie.expires
    } else {
      expires = Date.parse(new Date(Date.now() + this.options.expiration))
    }

    var defaults = {'data': stringData, 'expires': expires}
    if (this.options.extendDefaultFields) {
      defaults = this.options.extendDefaultFields(defaults, data)
    }

    var hui = (session) => {
      var changed = false
      Object.keys(defaults).forEach(function (key) {
        if (key === 'data') {
          return
        }

        if (session[key] !== defaults[key]) {
          session[key] = defaults[key]
          changed = true
        }
      })
      if (session.data !== stringData) {
        session.data = JSON.stringify(data)
        changed = true
      }
      if (changed) {
        session.expires = expires
        return this.sessionModel.update(JSON.parse(JSON.stringify(session)));
      }
      return session
    }
    var djopus = await this.sessionModel.findById(id);
    if (djopus)
      return hui(djopus);

    return this.sessionModel.create(Object.assign(defaults, {id})).then(hui)
  }

  SequelizeStore.prototype.touch = function touchSession (id, data, fn) {
    debug('TOUCH "%s"', id)
    var expires

    if (data.cookie && data.cookie.expires) {
      expires = data.cookie.expires
    } else {
      expires = Date.parse(new Date(Date.now() + this.options.expiration))
    }

    return this.sessionModel.updateAttributes({expires, id}).then(function (rows) {
      return rows
    })
  }

  SequelizeStore.prototype.destroy = function destroySession (id, fn) {
    debug('DESTROYING %s', id)
    return this.sessionModel.findById(id).then((session) => {
      // If the session wasn't found, then conider it destroyed already.
      if (session === null) {
        debug('Session not found, assuming destroyed %s', id)
        return null
      }
      return this.sessionModel.destroy(session)
    })
  }

  SequelizeStore.prototype.length = function calcLength (fn) {
    return this.sessionModel.count()
  }

  SequelizeStore.prototype.clearExpiredSessions = function clearExpiredSessions (fn) {
    debug('CLEARING EXPIRED SESSIONS')
    return this.sessionModel.destroy({where: {expires: {lt: Date.parse(new Date())}}})
  }

  SequelizeStore.prototype.startExpiringSessions = function startExpiringSessions () {
    // Don't allow multiple intervals to run at once.
    this.stopExpiringSessions()
    if (this.options.checkExpirationInterval > 0) {
      this._expirationInterval = setInterval(this.clearExpiredSessions.bind(this), this.options.checkExpirationInterval)
    }
  }

  SequelizeStore.prototype.stopExpiringSessions = function stopExpiringSessions () {
    if (this._expirationInterval) {
      clearInterval(this._expirationInterval)
    }
  }

  return SequelizeStore
}